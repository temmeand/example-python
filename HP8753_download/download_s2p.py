"""
download_s2p.py
===============

Download s2p data from an HP 8753D Vector Network Analyzer.

This file downloads two port data from an HP 8753 vector network analyzer and
saves it to a touchstone (s2p) file. It then loads the data back in and plots
the data using Scikit-RF. A measurement is taken for a S parameter, the data
downloaded in real, imaginary format using an ASCII data transfer, and then the
process is repeated for the next S parameter. This is done because I am unaware
of how to download all of the S parameters without displaying and measuring each
one individually. Because of this long acquisition time, the software may not
be suitable for a dynamic system (in fact, the 8753 may not be suitable --- an
ENA or PNA would be faster).

Data is saved to ./Data/fileName-timestamp.s2p where `fileName` is defined below
and the s2p extension is automatically added. A time stamp is added so that the
file name does not have to be changed every time.

This should be ran as a script. No setup is done by the script. Search for
ChangeME to find the section of code that contains the following which may need
to be adjusted:

    - The file name for the data to be saved under, `fileName`.
    - The variable `numGrps` should be adjusted to match the averaging factor of
    the analyzer. Set to 1 if no averaging.
    - The GPIB address of the analyzer is defined in the assignment of `ena`.
    - The option line of the s2p file may need to be adjusted if some settings
    are changed such as the impedance.
    - Likewise, the port impedance in the s2p file may need to be changed at the
    second occurrence of ChangeME

A dictionary is used for the commands. This is done with the hope of making it
easier to port to a different analyzer. To date, this has be done for an
HP8510C.

Note
----

Under some cases, this will cause the analyzer to hang and not respond to any
front panel buttons. The only solution seems to be a hard reset of the analyzer.
The cause of this is unknown. It only started happening recently. It may have
something to do with the calibration and asking the analyzer if calibration is
turned on. If you experience problems, began by commenting out all lines of code
and then adding them in one-by-one until the problem occurs. Please let me know
if you find something.

File Info
---------

Original Date: 13-Mar-2013
Updated: 08-Dec-2014

Andrew Temme
temmeand@gmail.com
Electromangetics Research Group
Michigan State University

"""

from __future__ import division

import visa
import numpy as np
from datetime import datetime
from os import path, makedirs
import skrf

__version__ = 1.0
__author__ = 'Andrew Temme'
__date__ = '12/2014'

print "start"
#------------------------------------------------------------------------------
# ChangeME
fileName = 'scratch'
numGrps = 4
ena = visa.instrument('GPIB::16', timeout = 120)

# The option line at the top of the s2p file may need to be changed if you
# adjust the system impedance or things like that.
optLine = "# Hz S RI R 50"
#------------------------------------------------------------------------------
idn =  ena.ask('*IDN?')
print idn

cmd8753D = {\
'basicInit':'HOLD;DUACOFF;CHAN1;S11;LOGM;CONT;AUTO',\
'corrQ':'CORR?',\
'freqSpanQ':'SPAN?',\
'freqStartQ':'STAR?',\
'freqStopQ':'STOP?',\
'getImag':'IMAG;OUTPFORM',\
'getLinMag':'LINM;OUTPFORM',\
'getLogMag':'LOGM;OUTPFORM',\
'getPhase':'PHAS;OUTPFORM',\
'getReal':'REAL;OUTPFORM',\
'hold':'HOLD',\
'IDStr':'HEWLETT PACKARD,8753D,0,6.14',\
'ifbwQ':'IFBW?',\
'numPtsQ':'POIN?',\
'powerQ':'POWE?',\
'preset':'PRES',\
'numGroups':'NUMG',\
'polar':'POLA',\
's11':'S11',\
's21':'S21',\
's12':'S12',\
's22':'S22'\
}

cmdDict = cmd8753D

#------------------------------------------------------------------------------
ena.write('form4')
numPts = ena.ask_for_values(cmdDict['numPtsQ'])[0]
freqStart = ena.ask_for_values(cmdDict['freqStartQ'])[0]
freqStop = ena.ask_for_values(cmdDict['freqStopQ'])[0]
freq = np.linspace(freqStart,freqStop,num=numPts,endpoint=True)
ifbw = ena.ask_for_values(cmdDict['ifbwQ'])[0]
pwr = ena.ask_for_values(cmdDict['powerQ'])[0]
corr = ena.ask(cmdDict['corrQ'])

dateString = datetime.now().strftime("%Y-%m-%d")
timeString = datetime.now().strftime("%H:%M:%S")

dataDir = 'Data/' + dateString
if not path.exists(dataDir):
    makedirs(dataDir)

print "here"
s11polar = np.array(ena.ask_for_values(cmdDict['s11']+cmdDict['polar']+';'+cmdDict['numGroups'] + str(numGrps)+';outpform'))
print "there"
s11polReal = s11polar[::2]								# real values from the polar data
s11polImag = s11polar[1::2]								# imaginary values from the polar data

print "s21"
s21polar = np.array(ena.ask_for_values(cmdDict['s21']+cmdDict['polar']+';'+cmdDict['numGroups'] + str(numGrps)+';outpform'))
s21polReal = s21polar[::2]								# real values from the polar data
s21polImag = s21polar[1::2]								# imaginary values from the polar data

print "s12"
s12polar = np.array(ena.ask_for_values(cmdDict['s12']+cmdDict['polar']+';'+cmdDict['numGroups'] + str(numGrps)+';outpform'))
s12polReal = s12polar[::2]								# real values from the polar data
s12polImag = s12polar[1::2]								# imaginary values from the polar data

print "s22"
s22polar = np.array(ena.ask_for_values(cmdDict['s22']+cmdDict['polar']+';'+cmdDict['numGroups'] + str(numGrps)+';outpform'))
s22polReal = s22polar[::2]								# real values from the polar data
s22polImag = s22polar[1::2]								# imaginary values from the polar data

visa.vpp43.gpib_control_ren(ena.vi,2)
#saveData = s22polar
saveData = np.concatenate(([freq],
                           [s11polReal],[s11polImag],
                           [s21polReal],[s21polImag],
                           [s12polReal],[s12polImag],
                           [s22polReal],[s22polImag])).T


touchFileName = dataDir + "/" + fileName + ".s2p"
print touchFileName
saveFile = open(touchFileName, "w")
saveFile.write("!"+idn+"\n")
saveFile.write("!Date: " + dateString + " " + timeString + "\n")
saveFile.write("!Data & Calibration Information:\n")
if corr == '0':
    saveFile.write("!Freq S11 S21 S12 S22\n")
elif corr== '1':
    saveFile.write("!Freq S11:Cal(ON) S21:Cal(ON) S12:Cal(ON) S22:Cal(ON)\n")

# ChangeME
saveFile.write("!PortZ Port1:50+j0 Port2:50+j0\n")
saveFile.write(("!Above PortZ is port z conversion or system Z0 "
                "setting when saving the data.\n"))
saveFile.write(("!When reading, reference impedance value at option "
                "line is always used.\n"))
saveFile.write("!\n")
saveFile.write("!--Config file parameters\n")
saveFile.write("!start = " + str(freqStart) + "\n")
saveFile.write("!stop = " + str(freqStop) + "\n")
saveFile.write("!numPts = " + str(numPts) + "\n")
saveFile.write("!avgFact = " + str(numGrps) + "\n")
saveFile.write("!power = " + str(pwr) + "\n")
saveFile.write("!ifbw = " + str(ifbw) + "\n")
saveFile.write("!\n")
saveFile.write(optLine + "\n")
np.savetxt(saveFile,saveData,delimiter=" ")
saveFile.close()

ntwk = skrf.Network(touchFileName)
ntwk.plot_s_db()
legend(loc=0)
